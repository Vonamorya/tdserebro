$(document).ready(function () {

    (function ($) {

        $('#filterGroup').keyup(function () {

            var rex = new RegExp($(this).val(), 'i');
            $('.searchableGroup a').hide();
            $('.searchableGroup a').filter(function () {
                return rex.test($(this).text());
            }).show();

        })

    }(jQuery));

});

$(document).ready(function () {

    (function ($) {

        $('#filterMaterial').keyup(function () {

            var rex = new RegExp($(this).val(), 'i');
            $('.searchableMaterial a').hide();
            $('.searchableMaterial a').filter(function () {
                return rex.test($(this).text());
            }).show();

        })

    }(jQuery));

});

$(document).ready(function () {

    (function ($) {

        $('#filterVstavka').keyup(function () {

            var rex = new RegExp($(this).val(), 'i');
            $('.searchableVstavka a').hide();
            $('.searchableVstavka a').filter(function () {
                return rex.test($(this).text());
            }).show();

        })

    }(jQuery));

});